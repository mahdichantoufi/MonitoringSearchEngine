#Catalogue PDF - Web:

Ceci est un module de recherche appliqué à la base de données 
remplie préalablement par le contenu de fichiers PDF.

Ce module Web est développé en 
NodeJS/knex/Postgres pour le backend 
et 
Javasript/HTML/CSS/BootSwatch pour le frontend




Pour l'intégration de ce module, vous devez :

  - Modifier les variables d'identification à la BDD dans le fichier d'environnement
          path: PROJECTROOT/MonitoringPDFSearchEngine/backend/.env
  - Dans le même fichier, renseigner le port souhaité pour la BDD. 
        L'outils d'administration de la BDD sera sur le port 8080, modifiable sur le ficher suivant : PROJECTROOT/MonitoringPDFSearchEngine/backend/docker-copose.yml.
  - Dans le fichier ./backend/src/index.js , renseigner le port souhaité pour l'API Express.
  - Dans le même fichier, renseigner votre clé Google API traduction



  - Télécharger toutes les dépendance en lançant les commandes suivantes:

          cd PROJECTROOT/MonitoringPDFSearchEngine;
          npm i;
          cd ./backend;
          npm i

  - Télécharger et Installer Docker : https://www.docker.com/

  - Vous pouvez ensuite allumer les contenaires Docker configurés sur ce projet en tapant la commande suivante : 
 
          cd PROJECTROOT/MonitoringPDFSearchEngine/backend;
          docker-compose up
          (docker-compose down pour l'effet inverse)

  - Construire la BDD, les tables et les liens en tapant la commande :
  
          cd PROJECTROOT/MonitoringPDFSearchEngine/backend;
          npm run migrate
  
  - Remplir les premières données ainsi que celles qui ne bougents pas :
  
          cd PROJECTROOT/MonitoringPDFSearchEngine/backend;
          npm run seed

  - Insérer les documents dans le repertoire PDFs_to_insert à l'aide du script :
  
          cd PROJECTROOT/MonitoringPDFSearchEngine/backend;
          npm run insertpdfs
